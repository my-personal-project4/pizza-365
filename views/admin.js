$(document).ready(function(){
  //Region 1: vùng khai báo biến toàn cục
  var gId;
  var gOrderId;
  var gPizzaSize = ["S","M","L"];
  // var gDataOrder1 = [];
  var gDataOrder = [];// khai báo biến mảng để chứa thông tin đơn hàng
  const gORDER = ["orderCode", "kichCo","loaiPizza","maNuocUong", "thanhTien", "hoTen","soDienThoai","trangThai", "action"];
  //khai báo cột của datatable
  const gORDER_ID = 0;
  const gKICH_CO = 1;
  const gLOAI_PIZZA = 2;
  const gID_NUOC_UONG = 3;
  const gTHANH_TIEN= 4;
  const gHO_TEN = 5;
  const gSO_DIEN_THOAI= 6;
  const gTRANG_THAI = 7;
  const gACITON = 8;
  //định nghĩa datatable(chưa có dữ liệu)
  $("#tbl-order").DataTable({
    // stateSave: true,
    columns:[
      {data: gORDER[gORDER_ID]},
      {data: gORDER[gKICH_CO]},
      {data: gORDER[gLOAI_PIZZA]},
      {data: gORDER[gID_NUOC_UONG]},
      {data: gORDER[gTHANH_TIEN]},
      {data: gORDER[gHO_TEN]},
      {data: gORDER[gSO_DIEN_THOAI]},
      {data: gORDER[gTRANG_THAI]},
      {data: gORDER[gACITON]},
    ],
    columnDefs:[
      {
        targets: gACITON,
        defaultContent: `
        <button class="btn btn-link btn-edit" data-toggle="tooltip" data-placement="bottom" title="Edit Order"><i class="text-info fas fa-edit"></i></button>
        <button class="btn btn-link btn-sm btn-delete" data-toggle="tooltip" data-placement="bottom" title="Delete Order"><i class="fas fa-trash text-danger"></i></button>
        `
      }, 
     
    ],
    
  })
  //Region 2: vùng gán thực thi hàm xử lý sự liện
  onPageLoading();
  $("#tbl-order").on("click",".btn-edit",function(){
    onBtnDetailClick(this);// this là button được ấn
  });
  $("#tbl-order").on("click",".btn-delete",function(){
    onBtnDeleteClick(this);// this là button được ấn
  });
  $("#btn-filter-order").on("click", onBtnFilterClick);
  loadDrinkList();
  getPizzaSize(gPizzaSize);
  $("#btn-confirm").on("click", onBtnConfirmClick);
  $("#btn-delete-order").on("click", onBtnDeleteOrderClick);
  $("#btn-cancel").on("click", onBtnCancelClick);
  $("#btn-add-order").on("click", onBtnAddOrderClick);
  $("#btn-create").on("click", onBtnCreateOrderClick);
  $("#select-create-pizzasize").on("change",onBtnChangeClick);
  $("#inp-create-voucherid").on("change",onBtnChangeInputClick);
  
  //Region 3: vùng khai báo các hàm xử lý sự kiện
  function onPageLoading(){
    //lấy data từ api
    $.ajax({
      url: "/orders",
      type: "GET",
      dataType: "json",
      success:function(res){
        gDataOrder = res;
        console.log(gDataOrder);
        loadDataToTable(gDataOrder.data);
      },
      error: function(error){
        console.assert(error.responseText);
      }
    });
  }

  function onBtnFilterClick(){
    "use strict";
    var vStatus = $("#select-status").val();
    var vPizzaType = $("#select-type-pizza").val();
    let filterArray = gDataOrder.data;
    console.log(filterArray);
    var vDataFilter = filterArray.filter(paramOrder => {
          return((vStatus === "all" || (paramOrder.trangThai != null && vStatus.toUpperCase() === paramOrder.trangThai.toUpperCase()))
          && (vPizzaType === "all" || (paramOrder.loaiPizza != null && vPizzaType.toUpperCase() === paramOrder.loaiPizza.toUpperCase())))
          
      });
      console.log(vDataFilter)
    loadDataToTable(vDataFilter);
  }
  
  //hàm xử lí khi nút confirm được click
  function onBtnConfirmClick(){
    console.log("ID: " + gId);
    var vObjectRequest = {
          trangThai: "",
          id: 0,
          orderId: "",
          kichCo: "",
          duongKinh: "",
          loaiPizza: "",
          maVoucher: "",
          giamGia: "",
          maNuocUong: "",
          soLuongNuoc: "",
          hoTen: "",
          email: "",
          soDienThoai: "",
          loiNhan: "",
          ngayTao: "",
          ngayCapNhat: "",
    }
    //b1: thu thập dữ liệu
    getData(vObjectRequest);
    console.log(vObjectRequest);
    //b2 check dữ liệu
    //b3 call api
    putApi(vObjectRequest);
    //b4: load lại dữ liệu để kiểm tra cập nhật
    // reloadDataToTable(vObjectRequest);
  }

  //hàm xử lí khi nút cancel được click

  function onBtnCancelClick(){
    $("#infor-order-modal").modal("hide");
  }
  
  //Region 4: vùng khai báo hàm dùng chung
  function loadDataToTable(paramObj){
    "use strict";
    var vTable = $("#tbl-order").DataTable();
    vTable.clear();
    vTable.rows.add(paramObj);
    vTable.draw();
  }

  function onBtnDetailClick(paramButton){
    "use strict";
    var vRowSelect = $(paramButton).closest("tr");
    var vTable = $("#tbl-order").DataTable();
    var vDataRow = vTable.row(vRowSelect).data();
    gId = vDataRow._id;
    gOrderId = vDataRow.orderCode;
    console.log(gId);
    console.log(gOrderId);
    getDetailOrderApi(gId);
    $("#infor-order-modal").modal("show");
  }
  
  //hàm khi click nút biểu tượng thùng rác
  function onBtnDeleteClick(paramButton){
    "use strict";
    var vRowSelect = $(paramButton).closest("tr");
    var vTable = $("#tbl-order").DataTable();
    var vDataRow = vTable.row(vRowSelect).data();
    gId = vDataRow._id;
    console.log(gId);
    gOrderId = vDataRow.orderId;
    $("#delete-confirm-modal").modal("show");
  }

  //hàm khi xác nhận xóa order
  function onBtnDeleteOrderClick(){
    "use strict";
  $.ajax({
    async:false,
    url: "/orders/"  + gId,
    type:"DELETE",
    dataType: "json",
    success:function(res){
      alert("đã xóa thành công order có ID=" + gId);
      $("#delete-confirm-modal").modal("hide");
      location.reload();
    },
    error: function(ajaxContext){
      console.log(ajaxContext.responseText);
    }
  })
  }

  //hàm khi click nút thêm mới
  function onBtnAddOrderClick(){
    $("#create-order-modal").modal("show");
    $("#inp-create-status").val("open")
   
  }

  //hàm xử lý khi chọn kích cỡ pizza
  function onBtnChangeClick(){
    var vSelect = $("#select-create-pizzasize").val();
    if (vSelect == "S") {
      $("#inp-create-duong-kinh").val("20");
      $("#inp-create-suon-nuong").val("2");
      $("#inp-create-salad").val("200");
      $("#inp-create-number-drink").val("2");
      $("#inp-create-total-price").val("150000");
    } else if(vSelect == "M") {
      $("#inp-create-duong-kinh").val("25");
      $("#inp-create-suon-nuong").val("4");
      $("#inp-create-salad").val("300");
      $("#inp-create-number-drink").val("3");
      $("#inp-create-total-price").val("200000");
    } else {
      $("#inp-create-duong-kinh").val("30");
      $("#inp-create-suon-nuong").val("8");
      $("#inp-create-salad").val("500");
      $("#inp-create-number-drink").val("4");
      $("#inp-create-total-price").val("250000");
    }
  }

  //hàm xử lý khi chọn mã giảm giá
  function onBtnChangeInputClick(){
    var vVoucherId = $("#inp-create-voucherid").val();
    var vThanhTien = $("#inp-create-total-price").val()
    $("#inp-create-discount").val(discountPrice(vThanhTien, checkDiscount(vVoucherId)));
  }

  //hàm trả về phần trăm giảm giá
  function discountPrice(paramPrice, paramDiscountPercent) {
    "use strict";
    // var vLastPrice = -1;
    var vPercent = paramDiscountPercent / 100 * paramPrice;
    // vLastPrice = paramPrice - paramPrice * vPercent;
    return vPercent;
  }

  // hàm kiểm tra mã giảm giá
  function checkDiscount(paramDiscountStr) {
    var vDiscountPercent = 0;
    if(paramDiscountStr != ""){
    $.ajax({
        async: false,
        url: "/voucherCode?maVoucher=" +
        paramDiscountStr,
        type: "GET",
        success: function (res) {
        console.log(res);
        if(res.data === null) {
            vDiscountPercent = 0;
        }else {
            vDiscountPercent = res.data.phanTramGiamGia;
        }    
        },
        error: function(ajaxContext){
        alert("mã giảm giã không tồn tại");
        vDiscountPercent = 0;
        }
    });
    }

    return vDiscountPercent;
}
  

  //Hàm xử lý để tạo mới order pizza
  function onBtnCreateOrderClick(){
    "use strict";
    var vCreateOrderObj = {
      kichCo: "",
      duongKinh: "",
      suon: "",
      salad: "",
      loaiPizza: "",
      maVoucher: "",
      maNuocUong: "",
      soLuongNuoc: "",
      hoTen: "",
      thanhTien: "",
      email: "",
      soDienThoai: "",
      diaChi: "",
      loiNhan: "",
    }
    //b1: thu thập dữ liệu
    getCreateData(vCreateOrderObj);
    console.log(vCreateOrderObj);
    //b2 check dữ liệu
    var vCheck = validateData(vCreateOrderObj);
    if(vCheck) {
      //b3 call api
      postApi(vCreateOrderObj);
    }
  }
  //Hàm load danh sách đồ uống
  function loadDrinkList() {
    $.ajax({
      url: "/devcamp-pizza365/drinks",
      contentType: "json",
      type: "GET",
      success: function (res) {
        console.log(res);
        loadDrinkOption(res.data);
      },
    });
  }

  //Hàm gán dữ liệu vào ô select drink
  function loadDrinkOption(paramDrinkObj) {
    Array.from(paramDrinkObj).forEach((drink) => {
      $("#select-drink").append(
        `<option value='${drink.maNuocUong}'>${drink.tenNuocUong}</option>`
      );
    });
    Array.from(paramDrinkObj).forEach((drink) => {
      $("#select-create-drink").append(
        `<option value='${drink.maNuocUong}'>${drink.tenNuocUong}</option>`
      );
    });
  }
  
  //hàm tạo option cho select pizza
  function getPizzaSize(paramPizza){
    "use strict";
    $.each(paramPizza,function(i,item){
      $("#select-pizzasize").append($('<option>',{
        text: item,
        value: item
      }))
    })

    $.each(paramPizza,function(i,item){
      $("#select-create-pizzasize").append($('<option>',{
        text: item,
        value: item
      }))
    })
  }

  //hàm gọi api order theo order id
  function getDetailOrderApi(paramOrderId){
    "use strict";
    $.ajax({
      url: "/orders/" + paramOrderId,
      type:"GET",
      dataType:"json",
      success: function(res){
        hanldeOrderDetail(res.data);
        console.log(res);
      },
      error: function(error){
        alert(error.responseText);
      }
    })
  }

  //hàm đổ data vào modal
  function hanldeOrderDetail(paramResponse){
    "use strict";
    $("#inp-id").val(paramResponse._id);
    $("#inp-orderid").val(paramResponse.orderCode);
    $("#select-pizzasize").val(paramResponse.kichCo);
    $("#inp-duong-kinh").val(paramResponse.duongKinh);

    $("#inp-salad").val(paramResponse.salad);
    $("#inp-suon-nuong").val(paramResponse.suon);
    $("#inp-type-pizza").val(paramResponse.loaiPizza);
    $("#inp-voucherid").val(paramResponse.maVoucher);

    $("#inp-total-price").val(paramResponse.thanhTien);
    $("#inp-discount").val(paramResponse.giamGia);
    $("#select-drink").val(paramResponse.maNuocUong);
    $("#inp-number-drink").val(paramResponse.soLuongNuoc);

    $("#inp-fullname").val(paramResponse.hoTen);
    $("#inp-email").val(paramResponse.email);
    $("#inp-phone").val(paramResponse.soDienThoai);
    $("#inp-address").val(paramResponse.diaChi);
    $("#inp-message").val(paramResponse.loiNhan);
    $("#inp-status").val(paramResponse.trangThai);

    // $("#inp-create-date").val(paramResponse.ngayTao);
    // $("#inp-update-date").val(paramResponse.ngayCapNhat);
    $("#inp-create-date").val(moment(paramResponse.ngayTao).format("DD-MM-YYYY"));

    $("#inp-update-date").val(moment(paramResponse.ngayCapNhat).format("DD-MM-YYYY"))
  }

  //hàm thu thập dữ liệu order trên update modal
  function getData(paramObj){
    "use strict";
    paramObj._id=  $("#inp-id").val()
    paramObj.orderCode=  $("#inp-orderid").val()
    paramObj.kichCo=  $("#select-pizzasize").val()
    paramObj.trangThai=  $("#select-update-status").val()
    paramObj.duongKinh=  $("#inp-duong-kinh").val();

    paramObj.salad = $("#inp-salad").val();
    paramObj.suon = $("#inp-suon-nuong").val();
    paramObj.loaiPizza = $("#inp-type-pizza").val();
    paramObj.maVoucher = $("#inp-voucherid").val();

    paramObj.thanhTien = $("#inp-total-price").val();
    paramObj.giamGia = $("#inp-discount").val();
    paramObj.maNuocUong = $("#select-drink").val();
    paramObj.soLuongNuoc = $("#inp-number-drink").val();
    paramObj.hoTen = $("#inp-fullname").val();
    paramObj.email = $("#inp-email").val();
    paramObj.soDienThoai = $("#inp-phone").val();
    paramObj.diaChi = $("#inp-address").val();
    paramObj.loiNhan = $("#inp-message").val();

    // paramObj.ngayTao = $("#inp-create-date").val().moment().;
    // paramObj.ngayCapNhat = $("#inp-update-date").val();
  }

  //hàm thu thập dữ liệu order trên create modal
  function getCreateData(paramObj){
    "use strict";
    paramObj.kichCo=  $("#select-create-pizzasize").val()
    paramObj.duongKinh=  $("#inp-create-duong-kinh").val();

    paramObj.salad = $("#inp-create-salad").val();
    paramObj.suon = $("#inp-create-suon-nuong").val();
    paramObj.loaiPizza = $("#select-create-pizzatype").val();
    paramObj.maVoucher = parseInt($("#inp-create-voucherid").val());

    paramObj.thanhTien = $("#inp-create-total-price").val();
    paramObj.giamGia = $("#inp-create-discount").val();
    paramObj.maNuocUong = $("#select-create-drink").val();
    paramObj.soLuongNuoc = $("#inp-create-number-drink").val();
    paramObj.hoTen = $("#inp-create-fullname").val().trim();
    paramObj.email = $("#inp-create-email").val().trim();
    paramObj.soDienThoai = $("#inp-create-phone").val().trim();
    paramObj.diaChi = $("#inp-create-address").val().trim();
    paramObj.loiNhan = $("#inp-create-message").val().trim();
  }

  //hàm kiểm tra dữ liệu order trên create modal
  function validateData(paramObj){
    if(paramObj.duongKinh == ""){
      alert("Đường kính không được để trống");
      return false;
    }
    if(paramObj.salad == ""){
      alert("Salad không được để trống");
      return false;
    }
    if(paramObj.suon == ""){
      alert("Sườn không được để trống");
      return false;
    }
    if(paramObj.soLuongNuoc == ""){
      alert("Số lượng nước uống không được để trống");
      return false;
    }
    if(paramObj.thanhTien == ""){
      alert("Thành tiền không được để trống");
      return false;
    }
    if(paramObj.hoTen == ""){
      alert("Họ tên không được để trống");
      return false;
    }
    if(paramObj.soDienThoai == ""){
      alert("Số điện thoại không được để trống");
      return false;
    }
    if(paramObj.email!= "" && validateFormatEmail(paramObj.email) == false){
      alert("Email phải nhập đúng định dạng");
      return false;
    }
    if(paramObj.diaChi == ""){
      alert("Địa chỉ không được để trống");
      return false;
    }
    return true;
  }

  //hàm kiểm tra email
  function validateFormatEmail(param) {
    var vValidRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (param.match(vValidRegex)) {
      return true;
    } else {
      return false;
    }
  }

  //hàm  call api để create  order
  function postApi(paramObj){
    $.ajax({
      url: "/devcamp-pizza365/orders",
      type: "POST",
      async: false,
      data:JSON.stringify(paramObj),
      contentType: "application/json;charset=UTF-8",
      success:function(res){
        alert("tạo mới thành công order có orderCode: " + res.data.orderCode);
        $("create-order-modal").modal("hide");
        // reloadDataToTable(res.data);
        location.reload();
      },
      error: function(ajaxContext){
        alert(ajaxContext.responseText);
      }
    }) 
  }

  //hàm call api để update thông tin order
  function putApi(paramObj){
    $.ajax({
      url: "/orders/" + gId,
      type: "PUT",
      async: false,
      data:JSON.stringify(paramObj),
      contentType: "application/json;charset=UTF-8",
      success:function(res){
        console.log(res);
        alert("cập nhật trạng thái thành công ");
        $("#infor-order-modal").modal("hide");
        location.reload();
      },
      error: function(ajaxContext){
        alert(ajaxContext.responseText);
      }
    }) 
  }

  
  // function loadDataToTable1(params){
  //   "use strict";
  //   var vTable = $("#tbl-order").DataTable();
  //   vTable.clear();
  //   vTable.row.add(params.data);
  //   vTable.draw();
  // }
  // // hàm reload trang
  // function reloadDataToTable(paramId){
  //   $.ajax({
  //     url: "/orders/" + paramId._id,
  //     type: "GET",
  //     dataType: "json",
  //     success:function(res){
  //       gDataOrder1 = res;
  //       console.log(gDataOrder1);
  //       loadDataToTable1(gDataOrder1);
  //       setTimeout(function(){
  //         location.reload()
  //       },3000)
        
  //     },
  //     error: function(error){
  //       console.assert(error.responseText);
  //     }
  //   });
  // }
});